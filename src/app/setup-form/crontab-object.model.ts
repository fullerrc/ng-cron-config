export class CrontabObject {
    constructor (
        public jobName: string
        ,public jobMinute: number
        ,public jobHour: number
        ,public jobDayOfMonth: number
        ,public jobMonth: number
        ,public jobDayOfWeek: number
        ,public jobCommand: string
    ) {}
    // const newCrontabLine = (
    //     `# ${lineFields["jobName"]}\n`
    //     +`${lineFields.jobMinute} ${lineFields.jobHour} ${lineFields.jobDayOfMonth} `
    //     +`${lineFields.jobMonth} ${lineFields.jobDayOfWeek} `
    //     +`${lineFields.jobCommand}`
}