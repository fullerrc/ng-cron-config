import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { CrontabObject } from './crontab-object.model';

@Component({
  selector: 'app-setup-form',
  templateUrl: './setup-form.component.html',
  styleUrls: ['./setup-form.component.css']
})
export class SetupFormComponent implements OnInit {
  cronForm: FormGroup;

  predicatedInputs = {
    'doMinute': { name: 'Minute', formField: 'jobMinute' }
    , 'doHour': { name: 'Hour', formField: 'jobHour' }
    , 'doDayOfMonth': { name: 'Day (of month)', formField: 'jobDayOfMonth' }
    , 'doMonth': { name: 'Month', formField: 'jobMonth' }
    , 'doDayOfWeek': { name: 'Day (of week)', formField: 'jobDayOfWeek' }
  };

  predicatedInputsKeys = Object.keys(this.predicatedInputs);

  constructor() { }

  ngOnInit() {
    this.cronForm = new FormGroup({
      'doMinute': new FormControl(null)
      , 'doHour': new FormControl(null)
      , 'doDayOfMonth': new FormControl(null)
      , 'doMonth': new FormControl(null)
      , 'doDayOfWeek': new FormControl(null)
      , 'jobName': new FormControl(null)
      , 'jobCommand': new FormControl(null)
      , 'jobMinute': new FormControl(null)
      , 'jobHour': new FormControl(null)
      , 'jobDayOfMonth': new FormControl(null)
      , 'jobMonth': new FormControl(null)
      , 'jobDayOfWeek': new FormControl(null)
      , 'output': new FormControl(null)
    });
  }

  onSubmit () {
    const lineFieldsInput = {};
    const vals = this.cronForm.value;

    for (const field of this.predicatedInputsKeys) {
      const formFieldName = this.predicatedInputs[field].formField;
      lineFieldsInput[formFieldName] = vals[field] ? vals[formFieldName] : '*';
    }

    lineFieldsInput['jobName'] = vals.jobName;
    lineFieldsInput['jobCommand'] = vals.jobCommand;

    const lineFields: CrontabObject = <CrontabObject>Object.assign({}, lineFieldsInput);
    const newCrontabLine = (
      `# ${lineFields['jobName']}\n`
      + `${lineFields.jobMinute} ${lineFields.jobHour} ${lineFields.jobDayOfMonth} `
      + `${lineFields.jobMonth} ${lineFields.jobDayOfWeek} `
      + `${lineFields.jobCommand}`
    );
    // console.log(lineFields);
    console.log(newCrontabLine);

    this.cronForm.setValue({ ...this.cronForm.value, output: newCrontabLine });
  }
}
